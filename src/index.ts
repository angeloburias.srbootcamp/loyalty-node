import { AppDataSource } from "./data-source";
import { User } from "./entity/User"
import * as express from 'express';
import * as cors from 'cors';
import usersRouter from './route/user';
import authRouter from './route/auth';
import 'dotenv/config';

AppDataSource.initialize().then(async () => {

    var app = express();
    app.use(cors());
    app.use(express.json());

    app.use('/users', usersRouter);
    app.use('/auth', authRouter);

    app.listen(process.env.SERVER_PORT || 3000, () => {
        console.log('Server is running from port: ',
            process.env.SERVER_PORT || 3000);
    })
}).catch(error => console.log(error))

import { User } from './User';
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";

@Entity({ name: "tbl_auth" })
export class Auth {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 240 })
    username: string;

    @Column({ type: "varchar", length: 240 })
    password: string;

    @OneToOne(() => User)
    @JoinColumn()
    user: User;

    // isValidPassword = async (password: string) => {
    //     return await bcrypt.compare(password, this.Password);
    // }

    // setPassword = async (password: string) => {
    //     return await (this.Password = bcrypt.hashSync(password, 8));
    // }

    // hashPassword = async (plaintextPassword) => {
    //     const hash = await bcrypt.hash(plaintextPassword, 10);
    //     // Store hash in the database
    //     return this.Password = hash;
    // }

    // compare password
    // comparePassword = async (plaintextPassword, hash) => {
    //     const result = await bcrypt.compare(plaintextPassword, hash);
    //     return result;
    // }

    // generateJWT = () => {
    //     return jwt.sign(
    //         {
    //             Username: this.Username,
    //         },
    //         "SECRET",
    //         {
    //             expiresIn: "3"
    //         }
    //     )
    // }



} 
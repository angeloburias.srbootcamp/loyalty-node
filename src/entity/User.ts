import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"


@Entity({ name: "tbl_users" })
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 240 })
    firstName: string;

    @Column({ type: "varchar", length: 240 })
    lastName: string;

    @Column({ type: "varchar", length: 240 })
    email: string;

    @Column({ type: "varchar", length: 240 })
    phoneNumber: string;


}

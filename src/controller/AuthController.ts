import { User } from './../entity/User';
import { Auth } from './../entity/Auth';
import { Request, Response } from 'express';
import { BaseEntity } from 'typeorm';
import { AuthService } from '../service/AuthService';
import bcrypt = require('bcryptjs');
const authService = AuthService.createInstance();


export class AuthController {

    static createInstance() {
        return new AuthController();
    }

    default(req: Request, res: Response) {
        res.send({
            message: "default page: auth"
        });
    }

    login = async (req: Request, res: Response) => {
        const data = req.body;
        let auth = new Auth();
        auth.username = data.username;
        var hashedPassword = bcrypt.hashSync(data.password);
        var existing = bcrypt.compare(hashedPassword, data.password);
        console.log(existing);
        if (!hashedPassword)
            return res.status(400)
                .send({ message: "Something went wrong" });

        if (authService.findUser(hashedPassword)) {
            return res.send({
                message: "You are login",
                hash: hashedPassword
            });
        } else return res.send({
            message: "no user found"
        });

    }


}


import { AuthService } from './../service/AuthService';
import { Request, Response } from 'express';
import { UserService } from '../service/UserService';

const userService = UserService.createInstance();
const authService = AuthService.createInstance();

export class UserController {

    static createInstance() {
        return new UserController();
    }

    default = (req: Request, res: Response) => {
        res.send({
            message: "default page: users"
        });
    }

    create = async (req: Request, res: Response) => {
        const body = req.body;
        if (body) {
            const result = await userService.create(body)
                .catch(error => { return { error } });

            const result_creds = await authService.create(body)
                .catch(error => { return { error } });

            if (!result || result['error'])
                return res.status(400)
                    .send({ message: "Something went wrong" });

            return res.send({
                basic_info: result,
                login_creds: result_creds
            });
        }

        res.status(400).send({ message: "Something went wrong" });
    }

    update = async (req: Request, res: Response) => {
        const { id } = req.params;
        const body = req.body;
        const result = await userService.update(id, body)
            .catch(err => { return { err } });

        if (!result || result['err']) return res.status(400).send({ message: "Something went wrong" });

        res.send(result);
    }

    getList = async (req: Request, res: Response) => {
        const result = await userService.find()
            .catch(err => { return { err } });
        return await res.send(result);
    }

    getById = async (req: Request, res: Response) => {
        const { id } = req.params;
        const result = await userService.findById(id)
            .catch(err => { return { err } });

        if (!result || result['err']) return res.status(400).send({ message: "Something went wrong" });

        res.send(result);
    }

    delete = async (req: Request, res: Response) => {
        const { id } = req.params;
        const result = await userService.delete(id)
            .catch(err => { return { err } });

        if (!result || result['err']) return res.status(400).send({ message: "Something went wrong" });

        res.send(result);
    }

}
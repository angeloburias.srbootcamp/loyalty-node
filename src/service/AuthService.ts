import { User } from '../entity/User';
import { Auth } from '../entity/Auth';
import { UserDao } from '../dao/UserDao';
import { AuthDAO } from '../dao/AuthDAO';
import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";


// const bcrypt = require('bcryptjs');
const authDao = AuthDAO.createInstance();

export class AuthService {

    static createInstance() {
        return new AuthService;
    }

    create = async (data) => {
        const auth = new Auth();
        auth.username = data.username;
        var hashedPassword = bcrypt.hashSync(data.id, 8);
        auth.password = hashedPassword;
        return await authDao.save(auth);
    }

    find = async () => {
        return await authDao.find();
    }

    findUser = async (data) => {
        return await authDao.findByHash(data);
    }
}
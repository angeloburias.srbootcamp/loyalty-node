import { User } from '../entity/User';
import { Auth } from '../entity/Auth';
import { UserDao } from '../dao/UserDao';
import { AuthDAO } from '../dao/AuthDAO';
const bcrypt = require('bcryptjs');
const userDao = UserDao.createInstance();

const authDao = AuthDAO.createInstance();

export class UserService {

    static createInstance() {
        return new UserService;
    }

    create = async (data) => {
        const user = new User();

        user.firstName = data.firstName;
        user.lastName = data.lastName;
        user.email = data.email;
        user.phoneNumber = data.phoneNumber;

        let auth = new Auth();
        auth.username = data.username;
        auth.password = data.password;
        auth.user = user;
        await authDao.save(user);
        return await userDao.save(user);
    }

    find = async () => {
        return await userDao.find();
    }

    update = async (id, data) => {
        const user = await userDao.findOneByQuery(id);
        const input = { ...user, ...data }

        const userResult = await userDao.save(input);
        return userResult;
    }

    findById = async (id) => {
        return await userDao.findOneByQuery(id);
    }

    delete = async (id) => {
        const user = await userDao.findOneByQuery(id);
        return await userDao.remove(user);
    }
}
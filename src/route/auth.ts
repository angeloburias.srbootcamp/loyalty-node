
import * as express from 'express';
import { AuthController } from '../controller/AuthController';
const app = express();
const authController = AuthController.createInstance();

app.route('/').get(authController.default);
app.route('/login').post(authController.login);

export default app;


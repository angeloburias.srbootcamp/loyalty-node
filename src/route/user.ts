import { AuthController } from './../controller/AuthController';

import * as express from 'express';
import { UserController } from '../controller/UserController';
const app = express();
const usersController = UserController.createInstance();
const authController = AuthController.createInstance();

app.route('/').get(usersController.default);
app.route('/register').post(usersController.create);
app.route('/list').get(usersController.getList);
app.route('/edit/:id').patch(usersController.update);
app.route('/delete/:id').delete(usersController.delete);
app.route('/:id').get(usersController.getById);



export default app;


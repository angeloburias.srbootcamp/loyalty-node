
import { User } from '../entity/User'
import { AppDataSource } from '../data-source';

const userRepo = AppDataSource.getRepository(User);

export class UserDao {

    static createInstance() {
        return new UserDao();
    }

    save = async (data) => {
        return await userRepo.save(data);
    }

    find = async () => {
        return await userRepo.find();
    }

    findByID = async (id) => {
        return await userRepo.findOne(id);
    }

    async findOneByQuery(id) {
        return await userRepo
            .createQueryBuilder("tbl_auth")
            .leftJoinAndSelect("tbl_auth.user", "user")
            .where({
                id
            })
            .getOne();
    }

    remove = async (user) => {
        return await userRepo.remove(user);
    }
}
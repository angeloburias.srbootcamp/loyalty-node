
import { Auth } from '../entity/Auth';
import { AppDataSource } from '../data-source';

const authRepo = AppDataSource.getRepository(Auth);

export class AuthDAO {

    static createInstance() {
        return new AuthDAO();
    }

    save = async (data) => {
        return await authRepo.save(data);
    }

    find = async () => {
        return await authRepo.find();
    }

    findByID = async (id) => {
        return await authRepo.findOne(id);
    }

    findByHash = async (pwd) => {
        var result = authRepo
            .createQueryBuilder()
            .select("tbl_auth")
            .from(Auth, "tbl_auth")
            .where("tbl_auth.password= :password", { password: pwd })
            .getCount();

        return await result;
    }

    remove = async (user) => {
        return await authRepo.remove(user);
    }
}